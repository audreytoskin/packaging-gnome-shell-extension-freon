# RPM packaging of the GNOME Shell extension Freon

This package spec is for Freon, which has its own GNOME Shell
[extension page](https://extensions.gnome.org/extension/841/freon/)
and source code [repository](https://github.com/UshakovVasilii/gnome-shell-extension-freon).

Freon is a GNOME Shell extension for displaying the temperature of your
CPU, hard disk, solid state, and video card (NVIDIA, Catalyst, and
Bumblebee supported), as well as power supply voltage, and fan
speed. You can choose which HDD/SSD or other devices to include, what
temperature units to use, and how often to refresh the sensors readout,
and they will appear in the GNOME Shell top bar. For the GPU
temperature, you may need to install the vendor's driver for best
results.



## Bug reports and feature requests

Report any issues with Freon itself on the project's
[GitHub](https://github.com/UshakovVasilii/gnome-shell-extension-freon/issues).

Report issues specific to this package on the
[Red Hat Bugzilla](https://bugzilla.redhat.com/buglist.cgi?product=Fedora&component=gnome-shell-extension-freon).



## License

Everything specific to this repository uses the MIT License.

Freon itself uses the GNU GPL version 2.
